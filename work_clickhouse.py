import pandas as pd
from datetime import date
from clickhouse_driver import Client
from pandahouse import to_clickhouse

client = Client(host='localhost',
                settings={'use_numpy': True}
                # username='default',
                # database='dash_sales'
                )

# print(client.execute('SHOW DATABASES'))

# query_create_table = """
# DROP TABLE  IF  EXISTS dash_sales.sales
# CREATE TABLE IF NOT EXISTS  dash_sales.sales (
#        `Дата` Date  ,
#        `Количество` Float64,
#        `Цена` Float64,
#        `Себестоимость` Float64,
#        `Сумма` Float64,
#        `ВП` Float64,
#        `АкцияСсылка` String,
#        `НоменклатураНаименование` String,
#        `Направление` String,
#        `Категория` String,
#        `Группа` String,
#        `Подгруппа` String,
#        `ПодПодгруппа` String,
#        `ПодПодПодгруппа` String,
#        `СрокХраненияТовара` String,
#        `ТМ` String,
#        `ДеньНеделиНазвание` String,
#        `ДеньНедели` Int8,
#        `ДеньМесяца` Int8,
#        `Месяц` Int8,
#        `Год` Int32,
#        `ГодМесяц` String
#       , INDEX idx_ItemId `НоменклатураНаименование` TYPE set(0) GRANULARITY 8192
#       , INDEX idx_categori_1 `Направление` TYPE set(0) GRANULARITY 8192
#       , INDEX idx_categori_2 `Категория` TYPE set(0) GRANULARITY 8192
#       , INDEX idx_categori_3 `Группа` TYPE set(0) GRANULARITY 8192
#       , INDEX idx_categori_4 `Подгруппа` TYPE set(0) GRANULARITY 8192
#       , INDEX idx_categori_5 `ПодПодПодгруппа` TYPE set(0) GRANULARITY 8192
#       , INDEX idx_year `Год` TYPE set(0) GRANULARITY 8192
#       , INDEX idx_month `Месяц` TYPE set(0) GRANULARITY 8192
#       , INDEX idx_weekday `ДеньНедели` TYPE set(0) GRANULARITY 8192
#       , INDEX idx_daymonth `ГодМесяц` TYPE set(0) GRANULARITY 8192
#     )
#     ENGINE = MergeTree() PARTITION BY toYYYYMM(`Дата`) ORDER BY (`Дата`);
#     --INSERT INTO dash_sales.sales SELECT * FROM file('D:\DashPlotly\data\sales.csv', CSVWithNames, auto);
# """


# client.execute(query_create_table)


EPOCH_START = date(1970, 1, 1)
def get_count_days(d) -> int:
    return (date( int(d.split('-')[0]), int(d.split('-')[1]), int(d.split('-')[2])  ) - EPOCH_START).days

df = pd.read_csv(r"D:\DashPlotly\data\sales.csv",  sep=';')


df['Дата'] = df['Дата'].apply(get_count_days)

# df.to_csv(r"\wsl.localhost\Ubuntu\var\lib\sales_clickhouse.csv", index=False,  sep=';')



# client.execute(f"""INSERT INTO dash_sales.sales
# SELECT `Дата` ,
#        `Количество` ,
#        `Цена` ,
#        `Себестоимость` ,
#        `Сумма` ,
#        `ВП` ,
#        `АкцияСсылка` ,
#        `НоменклатураНаименование` ,
#        `Направление` ,
#        `Категория` ,
#        `Группа` ,
#        `Подгруппа` ,
#        `ПодПодгруппа` ,
#        `ПодПодПодгруппа` ,
#        `СрокХраненияТовара` ,
#        `ТМ` ,
#        `ДеньНеделиНазвание` ,
#        `ДеньНедели` ,
#        `ДеньМесяца` ,
#        `Месяц` ,
#        `Год`,
#        `ГодМесяц` FROM file('D:\DashPlotly\data\sales_clickhouse.csv', CSVWithNames, '`Дата` ,
#        `Количество` ,
#        `Цена` ,
#        `Себестоимость` ,
#        `Сумма` ,
#        `ВП` ,
#        `АкцияСсылка` ,
#        `НоменклатураНаименование` ,
#        `Направление` ,
#        `Категория` ,
#        `Группа` ,
#        `Подгруппа` ,
#        `ПодПодгруппа` ,
#        `ПодПодПодгруппа` ,
#        `СрокХраненияТовара` ,
#        `ТМ` ,
#        `ДеньНеделиНазвание` ,
#        `ДеньНедели` ,
#        `ДеньМесяца` ,
#        `Месяц` ,
#        `Год`,
#        `ГодМесяц`')
#        """)
connection = {'host': 'http://localhost:8123',
              'database': 'dash_sales'}

affected_rows = to_clickhouse(df, table='sales', connection=connection)
