import dash
from dash import dcc
from dash import html
import dash_bootstrap_components as dbc
import plotly.express as px
import pandas as pd
from dash.dependencies import Input, Output


import plotly.io as pio
pio.renderers.default = "browser"





# df = pd.read_csv(r"D:\DashPlotly\data\sales.csv", sep=';')
df = pd.read_csv(r"D:\DashPlotly\data\sales_less_category.csv", sep=';')

df['Дата'] = pd.to_datetime(df['Дата'])

cat_col = ['НоменклатураНаименование', 'Направление', 'Категория', 'Группа',
       'Подгруппа', 'ПодПодгруппа', 'ПодПодПодгруппа', 'ТМ', 'ДеньНеделиНазвание', 'ГодМесяц']

for el in cat_col:
    df[el] = df[el].astype('category')


price_selector = dcc.RangeSlider(
    id='range-price',
    min=min(df['Цена']),
    max=1000,
    marks={100: '100', 200: '200', 300: '300', 400: '400', 500:'500', 600:'600', 700:'700', 800:'800', 900:'900'},
    step=10,
    value=[0,1000]
    # value=[200, 500]
)

category_selector = dcc.Dropdown(
    id='category-select',
    options=[{'label': el, 'value': el} for el in df['Категория'].unique()],
    value=list(df['Категория'].value_counts().index[:3]),
    multi=True

)

year_selector = dcc.Dropdown(
    id='year-select',
    options=[{'label': el, 'value': el} for el in df['Год'].unique()],
    value=list(df['Год'].unique()),
    multi=True
)

month_name = ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь']
month_selector = dcc.Dropdown(
    id='month-select',
    options= month_name, #    [{'label': el, 'value': month_name[el-1]} for el in df['Месяц'].unique()],
    value=month_name,
    multi=True
)


# Инициализируем приложение
app = dash.Dash(__name__,
                external_stylesheets=[dbc.themes.COSMO])

app.layout = html.Div([
    dbc.Row(html.H2('Аналитика продаж по магазину'),
            style={}),

    dbc.Row([
        dbc.Col([
            html.Div('Диапозон цен товара:'),
            html.Div(price_selector)
        ], width={'size': 3}),
        dbc.Col([
            html.Div('Категории товаров:'),
            html.Div(category_selector)
        ], width={'size': 3,}),  # 'offset': 1
        dbc.Col([
            html.Div('Год:'),
            html.Div(year_selector)
        ], width={'size': 2}),
        dbc.Col([
            html.Div('Месяц:'),
            html.Div(month_selector)
        ], width={'size': 4}),
    ],style={'margin-bottom': 40}
    ),

    dbc.Row([
        dbc.Col([
            dcc.Graph(id='chart_count_price', )
            ], width={'size': 6, }),
        dbc.Col([
            dcc.Graph(id='chart_sumsales_dayweek', )
            ], width={'size': 6, }),
    ]),

    # dbc.Row([
    #     dbc.Col([
    #         dcc.DatePickerRange(
    #                 id='calendar-select',
    #                 # month_format='X',
    #                 # end_date_placeholder_text='X',
    #                 start_date=min(df['Дата']),
    #                 end_date=max(df['Дата'])
    #                 #    start_date_placeholder_text="Start Period",
    #                 #    end_date_placeholder_text="End Period",
    #                 #    calendar_orientation='vertical',
    #                 )
    #     ])
    # ])
],
style={
    'margin-left': '80px',
    'margin-rigtr': '80px',
})


@app.callback(
    Output(component_id='chart_count_price', component_property='figure'),
    [Input(component_id='range-price', component_property='value'),
     Input(component_id='category-select', component_property='value'),
     Input(component_id='year-select', component_property='value'),
     Input(component_id='month-select', component_property='value')]
    )
def update_chart_count_price(range_price, category, year, month): # range_price - передается 'value' из Input
    month_ind = [month_name.index(el) for el in month]
    # print(range_price)
    chart_data = df[  (df['Цена'] >=range_price[0])
                    & (df['Цена'] <=range_price[1])
                    & (df['Категория'].isin(category))
                    & (df['Год'].isin(year))
                    & (df['Месяц'].isin(month_ind))]
    fig = px.scatter(chart_data, x="Цена", y="Количество",
                     title='Соотношения количества продаж и цены на на товар',
                     labels={'Цена': 'Цена товара', 'Количество': 'Количество продаж за весь период'})
    return fig


@app.callback(
    Output(component_id='chart_sumsales_dayweek', component_property='figure'),
    [Input(component_id='range-price', component_property='value'),
     Input(component_id='category-select', component_property='value'),
     Input(component_id='year-select', component_property='value'),
     Input(component_id='month-select', component_property='value')]
    )
def update_chart_sumsales_dayweek(range_price, category, year, month): # range_price - передается 'value' из Input
    # print(range_price)
    month_ind = [month_name.index(el) for el in month]
    chart_data = df[  (df['Цена'] >=range_price[0])
                    & (df['Цена'] <=range_price[1])
                    & (df['Категория'].isin(category))
                    & (df['Год'].isin(year))
                     & (df['Месяц'].isin(month_ind))]
    fig = px.bar(chart_data.groupby(['ДеньНедели'])['Сумма'].mean(), y='Сумма',
                 title='Средняя выручка по дням недели',
                 x=['Пн.', 'Вт.', 'Ср.', 'Чт.', 'Пт.', 'Суб.', 'Вс'],
                 labels={'x': 'День недели', 'Сумма': 'Сумма выручки'})
    return fig









if __name__ == '__main__':
    app.run_server(debug=True) # Run the Dash app
